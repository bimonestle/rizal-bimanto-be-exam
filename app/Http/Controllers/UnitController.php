<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\UnitController;

class UnitController extends Controller
{
    public function GetUnit(){
        $unit = DB::table('PurwadhikaExam')->get();

        return view('unit.GetUnit', ['units' => $unit]);
    }
    public function AddUnit(){
        DB::beginTransaction(); //Dia gk akan dimasukkin ke database kalo belum2 di DB::commit
                                // Untuk ngerollback data/input jika ada kegagalan

        try{
            
            $this->validate($request, [
                'UnitName' => 'required',
                ]); //untuk validate si request

            $categoryname = $request->input('UnitName');


            // Pake metode Eloquent untuk simpen data ke database
            $unitbaru = new Unit;
            $unitbaru->Unit = $namaunit;
            $unitbaru->save();   // Untuk save data yang di add ke dala database

            DB::commit();
            return response()->json(["message" => "Adding New Unit Success !!"], 200); //response kalo berhasil
        }
        catch(\Exception $e){
            DB::rollback(); //jika insert mengalami kegagalan akan di-rollback see DB::beginTransaction
            return response()->json(["message" => $e->getMessage()], 500); //response kalo gk berhasil
        }
    }
    public function DeleteUnit(){
        $unit = DB::select('select * from units where active = ?', [1]);

        return view('user.DeleteUnit', ['units' => $unit]);
    }
}
